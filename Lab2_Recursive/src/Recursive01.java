import java.util.Scanner;

public class Recursive01 {
    public static void multiplication(int num){
        for(int i=1;i<=12;i++){
            int result = num * i;
            System.out.println(num + " x "+ i + " = " + result);
        }
    }
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int num = sc.nextInt();
        multiplication(num);

    }

}
