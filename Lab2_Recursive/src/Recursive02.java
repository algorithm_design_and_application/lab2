import java.util.Scanner;

public class Recursive02 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int num = sc.nextInt();
        int result = num % 2; 
        System.out.println(result);
    }
}
